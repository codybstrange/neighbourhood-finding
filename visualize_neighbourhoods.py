from shapely.geometry import Point, Polygon
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import json

# Output file name
out_name = "neighbourhoods.png"

# Load the background image - a simple map of NYC bounded by the coordinates below
img = plt.imread("nyc.png")

# Boundary box of the map
left, right, bottom, top = -74.03, -73.91, 40.62, 40.75

# Get the neighbourhood definitions from the common json file
with open('neighbourhoods.json', 'r') as json_file:
    neighbourhoods = json.load(json_file)

# Create the plot
fig, axis = plt.subplots(1, 1, figsize=(10, 8))
# We layer a simple map of NYC under the neighbourhoods
axis.imshow(img, extent=[left, right, bottom, top], aspect='auto')

for i, key in enumerate(neighbourhoods.keys()):
    corners = [[coords[1], coords[0]] for coords in neighbourhoods[key]]
    poly = Polygon(corners)
    # Create a Polygon patch from the vertices
    # This line is the shading of the neighbourhood
    polygon = patches.Polygon(corners, closed=True, color=f'C{i}', alpha=0.75)  # `alpha` controls transparency
    # Add the polygon to the plot
    axis.add_patch(polygon)
    # Draw the border of the neighbourhood
    x, y = poly.exterior.xy
    axis.plot(x, y, label=key, color=f"C{i}", lw=1.4)

# Set the limits of the plot
axis.set_xlim(left, right)
axis.set_ylim(bottom, top)
# Label the axes
axis.set_xlabel('Longitude')
axis.set_ylabel('Latitude')
axis.set_title("Neighbourhoods")
# Set the font size of the legend
axis.legend(prop={'size': 10})
fig.savefig(out_name, dpi=300, bbox_inches="tight")
