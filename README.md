# neighbourhood-finding

This code is used for defining taking in some pieces of data and deciding which neighbourhood they belong to.

The neighbourhood definition is implemented via a JSON file - `neighbourhoods.json`. If the coordinates are lists of lists, there is a converter script - `convert_coordinates_to_json.py` (see code for an example of a set of neighbourhoods of Brooklyn). Note that the neighbourhood is defined as a series of coordinates that are the corners of a bounding polygon, and as such, the coordinates must be entered in a cyclic fashion, i.e. as if one is _walking around the perimeter_ of the bounding polygon (the direction and the starting corner are unimportant). If the coordinates are not entered in this fashion, you will end up with spurious results, as demonstrated by the neighbourhood Red Hook in the output of `visualize_neighbourhoods.py`.

There are three scripts: 
- `label_neighbourhoods.py` which takes an input excel spreadsheet (and a particular sheet of this spreadsheet) and corrects incorrect coordinate entries, and then assigns each row to a neighbourhood if possible. The output is a tsv file with the data only modified to have correct coordinates and a neighbourhood column.
- `image_neighbourhoods.py` which takes a single folder of uniquely named images (there is no known constraint on the type of image, i.e. jpg vs png vs etc.), and then examines the metadata (where possible) to extract the GPS coordinates and then attempt to assign a neighbourhood to the image. The output is a tsv file with the file-names and the associated neighbourhood.
- `visualize_neighbourhoods.py` which takes a background image `nyc.png` and layers the proposed neighbourhoods in the JSON file on top as a visual guide to what the neighbourhoods will look like.
- `nyc_map.py` produces an interactive map of the neighbourhoods and the SSIDs recorded that can be run in any modern browser. The program can generate the map with or without the SSIDs. For just the neighbourhoods, run `python nyc_map.html`, or if you want the SSIDs also there, run `python nyc_map.html --input <ssid_file_name> --sheet_name <Name of Excel Sheet>`. If the SSIDs are a `.tsv` or `.csv` file, the sheet name does not need to be included.

## Quickstart

If you want to run the code, I recommend you use a Conda environment and install the various python packages inside this conda environment:
```bash
conda create --name neighbourhood
conda activate neighbourhood
conda install -c conda-forge pandas numpy folium contextily matplotlib geopandas shapely pygmt
pip install PyExifTool
```

Alternatively, you can also install all these packages with `pip`, though `pip` is not recommended:
```bash
pip install pandas numpy folium contextily matplotlib geopandas shapely pygmt
```

## TODOs:
- [ ] Change the json structure to a geojson format
- [ ] Figure out how to embed the nyc_map.html inside an html page

## License
GNU v3.0

