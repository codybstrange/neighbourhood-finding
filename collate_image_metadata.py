from PIL import Image
from PIL.ExifTags import TAGS, GPSTAGS
import os
import pandas as pd
from shapely.geometry import Point, Polygon
import json
import argparse
import exiftool

# Function to determine the neighborhood of a point
def find_neighbourhood(neighbourhoods, latitude, longitude):
    point = Point(latitude, longitude)  # Shapely uses (longitude, latitude)
    for neighbourhood, corners in neighbourhoods.items():
        polygon = Polygon(corners)
        if polygon.contains(point):
            return neighbourhood
    return None

def get_metadata(args):
    # See if the input directory exists
    if not os.path.exists(args.input):
        raise Exception("Error in get_metadata - input folder not found")

    # Output dataframe
    out_df = pd.DataFrame(columns=["filename", "capture_date", "capture_time", "width", "height", "dimensions", "neighbourhood", "latitude", "longitude"])

    # Read in the neighbourhoods file
    if not os.path.exists(args.neighbourhoods):
        raise Exception("Error in get_metadata - neighbourhood definitions not found")

    # Get the neighbourhood definitions from the common json file
    with open(args.neighbourhoods, 'r') as json_file:
        neighbourhoods = json.load(json_file)

    # Loop over all the photos in the input directory
    files = os.listdir(args.input)
    images = [f for f in files if f.lower().endswith(('.png', '.jpg', '.jpeg'))]
    if len(images) == 0:
        raise Exception("No photos found in the given input directory")
    for i, file in enumerate(images):
        print(f"Processing image {i+1} of {len(images)}")
        file_path = os.path.join(args.input, file)
        metadata = exiftool.ExifToolHelper().get_metadata(file_path)[0]
        # Debug mode will print out the whole dictionary
        if args.debug:
            for k, v in metadata.items():
                print(f"{k}: {v}")
        # Get the values for the output dataframe
        width  = metadata["File:ImageWidth"]
        height = metadata["File:ImageHeight"]
        dimension = f"{width}x{height}"
        # The capture information of the metadata is composed of
        # two pieces: 'DATE TIME', with a whitespace separating the two
        # components
        capture_info = metadata["EXIF:CreateDate"].split(" ")
        capture_date = capture_info[0]
        capture_time = capture_info[1]
        
        # Latitude and Longitude have a reference.
        # TODO: What does the reference mean?
        lat_ref   = metadata["EXIF:GPSLatitudeRef"]
        latitude  = metadata["EXIF:GPSLatitude"]
        long_ref  = metadata["EXIF:GPSLongitudeRef"]
        longitude = metadata["EXIF:GPSLongitude"]
        neighbourhood = find_neighbourhood(neighbourhoods, latitude, longitude)
        out_df = pd.concat(
            [out_df, 
             pd.DataFrame(
             {"filename": [file.split(".")[0]], "capture_date": [capture_date], 
              "capture_time": [capture_time], "dimensions": [dimension], 
              "width": [width], "height": [height], 
              "latitude": [latitude], "longitude": [longitude],
              "neighbourhood": [neighbourhood]})])

    # Now df contains the photo filenames and their corresponding neighbourhood
    # Save the file as a tsv file 
    out_df.to_csv(os.path.join(args.input, "..", f"{args.output}.tsv"), sep="\t", index=False)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Optimize the parameters of SimChA")
    parser.add_argument('-I', "--input", type=str,
                        help="Absolute path to photo directory")
    parser.add_argument("-O", "--output", type=str,
                        help="Output TSV file. Will be saved at same level as input")
    parser.add_argument("-N", "--neighbourhoods", type=str,
                        default="./neighbourhoods.json",
                        help="Absolute path to the predefined neighbourhoods")
    parser.add_argument("-D", "--debug", type=bool, default=False,
                        help="Flag to turn on the print out of the metadata dictionary")
    args = parser.parse_args()

    get_metadata(args)
