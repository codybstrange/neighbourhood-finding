import folium
import json
from os.path import exists
from folium.features import DivIcon
from folium.plugins import MarkerCluster
import pandas as pd
import argparse
import os

wifi_icon_path = './wifi_icon.png'

# Function to calculate the center of a polygon (average of the coordinates)
def calculate_centroid(coords):
    num_points = len(coords)
    centroid_lat = sum([point[0] for point in coords]) / num_points
    centroid_lon = sum([point[1] for point in coords]) / num_points
    return [centroid_lat, centroid_lon]

def generate_map(out_filename, ssids):
    # Define the limits of the view we want to see
    west, south, east, north = -74.03, 40.62, -73.91, 40.75
    centre = [0.5*(north+south), 0.5*(east+west)]
    nyc_coordinates = centre

    # A map object centered on Brooklyn
    nyc_map = folium.Map(location=nyc_coordinates, zoom_start=13.25)

    ### Making the boroughs
    # Color scheme for the boroughs
    colours = [
        'red',
        'blue',
        'darkred',
        'lightred',
        'orange',
        'teal',
        'green',
        'darkgreen',
        'lightgreen',
        'darkblue',
        'lightblue',
        'purple',
        'darkpurple',
        'pink',
        'cadetblue',
        'turquoise',
        'lavender'
    ]
    # Get the boundaries of the boroughs
    with open("neighbourhoods.json", "r") as f:
        neighbourhoods = json.load(f)


    # Draw the boroughs
    for i, key in enumerate(neighbourhoods):
        print(f"Adding {key}")
        # Create the borough group and add the polygon
        borough_group = folium.FeatureGroup(name=key)
        folium.Polygon(locations=neighbourhoods[key], color=colours[i], fill=True).add_to(nyc_map)
        # Create the name of the borough
        borough_centroid = calculate_centroid(neighbourhoods[key])
        borough_markers = folium
        folium.map.Marker(
            borough_centroid,
            icon=DivIcon(
                html=f"""
                <div style="font-size: 16pt; font-weight: bold; color: #555; text-align: center; width: 150px;">
                    <span style="display:block; transform: translate(-50%, -50%);">{key}</span>
                </div>""",
            )
        ).add_to(borough_group)
        borough_group.add_to(nyc_map)
        # SSIDs are a separate group
        if len(ssids) > 0:
            print(f"Adding the SSIDs for {key}")
            borough_ssids = ssids.query("neighbourhood == @key")
            if len(borough_ssids) == 0:
                continue
            #wifi_icon = folium.CustomIcon(wifi_icon_path, icon_size=(8, 8)) 
            borough_ssids_group = MarkerCluster(name=f"{key} SSIDs", show=False).add_to(nyc_map)
            # Add the individual ssids from that borough to the group
            for _, ssid in borough_ssids.iterrows():
                folium.Marker([ssid.bestlat, ssid.bestlon], popup=ssid.ssid, icon=folium.DivIcon(html="""
        <div style="position: relative; width: 0; height: 0;">
            <div style="font-size: 24pt; color: #f2f2f2; background-color: #41657d; border-radius: 50%; width: 45px; height: 45px; 
                        display: flex; justify-content: center; align-items: center; position: absolute; bottom: 0; left: 0%; transform: translateX(-50%);">
                <i class="fa fa-wifi"></i>
            </div>
        </div>
    """)).add_to(borough_ssids_group)
            # Last, add the group to the map
            #borough_ssids_group.add_to(nyc_map)
        

    # Add additional tile layers for different styles with proper attribution
    folium.TileLayer('cartodbpositron').add_to(nyc_map)  # A clean map style

    # Stamen toner with attribution
    """folium.TileLayer(
        'stamentoner',
        name="Toner",
        attr='Map tiles by Stamen Design, under CC BY 3.0. Data by OpenStreetMap, under ODbL.'
    ).add_to(nyc_map)

# Stamen watercolor with attribution
    folium.TileLayer(
        'stamenwatercolor',
        name="Watercolor",
        attr='Map tiles by Stamen Design, under CC BY 3.0. Data by OpenStreetMap, under ODbL.'
    ).add_to(nyc_map)
    """
# Add a layer control panel to toggle between map styles
    folium.LayerControl().add_to(nyc_map)

# Save the map as an HTML file
    nyc_map.save(out_filename)

    print("Map has been generated and saved as 'nyc_map.html'. Open it in your browser to view it.")

# Simple function to read in an excel, tsv, or csv file
def read_file(file_path, sheet_name):
    # Check file exists:
    if not exists(file_path):
        raise ValueError("You tried to use an SSID file that could not be found. Make sure it is in the same directory as this script")


    # Get extension
    file_ext = os.path.splitext(file_path)[1].lower()
    if file_ext == ".xlsx":
        return pd.read_excel(file_path, sheet_name=sheet_name)
    elif file_ext == ".csv":
        return pd.read_csv(file_path)
    elif file_ext == ".tsv":
        return pd.read_csv(file_path, sep="\t")
    else:
        raise ValueError(f"Unsupported file extension: {file_ext}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="A simple python program to generate an interactive map of the Brooklyn area and its neighbourhoods in HTML format.")
    parser.add_argument("-o", "--out", type=str, default="nyc_map.html", help="Name of output HTML file. Default is nyc_map.html")
    parser.add_argument("-i", "--input_ssids", type=str, default="", help="Name of the input SSIDs file, including its extension (e.g. .xlsx or .tsv)")
    parser.add_argument("--sheet_name", type=str, default="", help="If the input SSID file is an Excel sheet, the program needs to know the sheet name that you wish to extract.")
    args = parser.parse_args()

        
    # Read in the SSIDs
    try:
        if args.input_ssids == "":
            ssids = pd.DataFrame()
        else:
            ssids = read_file(args.input_ssids, args.sheet_name)
        generate_map(args.out, ssids)
    except Exception as e:
        print(f"Error encountered: {e}")
