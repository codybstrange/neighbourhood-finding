from shapely.geometry import Point, Polygon
import pandas as pd
import json

# Excel spreadsheet details
name_of_file = "ssids.xlsx"
sheet_name = "field_trip_2023 merged"
# Name of the output file
out_name = "ssids_out.tsv"

# Get the neighbourhood definitions from the common json file
with open('neighbourhoods.json', 'r') as json_file:
    neighbourhoods = json.load(json_file)

## The bestlon and bestlat values for a lot of the samples have the wrong coordinates (leftover from SQL converstion)
# My idea: take all values with bestlon < -100 (and similarly bestlat > 100) and take the first two digits as the tens and units digits, and the rest
# as decimal places
def transform_value(value):
    if value < -100:
        # Convert to string for slicing
        value_str = str(int(abs(value)))
        # Take the first two digits and the rest, then construct the new value
        new_value = float(value_str[:2] + '.' + value_str[2:])
        # Return as negative
        return -new_value
    elif value > 100:
        # Convert to string for slicing
        value_str = str(int(abs(value)))
        # Take the first two digits and the rest, then construct the new value
        new_value = float(value_str[:2] + '.' + value_str[2:])
        # Return as negative
        return new_value
    else:
        return value


# Read in the excel spreadsheet
df = pd.read_excel(name_of_file, sheet_name)
# Apply the transformation to each value in the 'bestlon' and 'bestlat' columns
df['bestlon'] = df['bestlon'].apply(transform_value)
df["bestlat"] = df["bestlat"].apply(transform_value)


# Function to determine the neighborhood of a point
def find_neighbourhood(latitude, longitude):
    point = Point(latitude, longitude)  # Shapely uses (longitude, latitude)
    for neighbourhood, corners in neighbourhoods.items():
        polygon = Polygon(corners)
        if polygon.contains(point):
            return neighbourhood
    return None

# Apply the function to each row in the DataFrame
df["neighbourhood"] = df.apply(lambda row: find_neighbourhood(row['bestlat'], row['bestlon']), axis=1)
# Save the dataframe to a tsv file
df.to_csv(out_name, sep="\t", index=False)

nan_count = df["neighbourhood"].isna().sum()

print(f"The number of unassigned points: {nan_count}")

